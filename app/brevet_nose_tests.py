import nose
from datetime import timedelta
import arrow
from acp_times import open_time, close_time


def test_open_time():
    iso_example = '1981-04-05'
    iso_example2 = '2019-11-11'
    sixty_delta = timedelta(minutes=106)
    onetwentyfive_delta = timedelta(minutes=221)
    twohundred_delta = timedelta(minutes=353)
    fivefifty_delta = timedelta(minutes=1028)
    sixhundred_delta = timedelta(minutes=1128)
    assert open_time(0, 200, iso_example2) == arrow.get(iso_example2
                                                       ).isoformat()
    assert open_time(60, 200, iso_example) == (arrow.get(iso_example)
                                              + sixty_delta).isoformat()
    assert open_time(125, 200, iso_example2) == (arrow.get(iso_example2)
                                                 + onetwentyfive_delta
                                                ).isoformat()
    assert open_time(200, 200, iso_example) == (arrow.get(iso_example)
                                               + twohundred_delta
                                               ).isoformat()
    assert open_time(550, 600, iso_example2) == (arrow.get(iso_example2)
                                                + fivefifty_delta
                                                ).isoformat()
    assert open_time(600, 600, iso_example) == (arrow.get(iso_example)
                                                + sixhundred_delta
                                               ).isoformat() 
def test_close_time():
    iso_example = '1981-04-05'
    iso_example2 = '2019-11-11'
    sixty_delta = timedelta(minutes=240)
    onetwentyfive_delta = timedelta(minutes=500)
    twohundred_delta = timedelta(minutes=810)
    fivefifty_delta = timedelta(minutes=2200)
    sixhundred_delta = timedelta(minutes=2400)
    assert close_time(0, 200, iso_example2) == (arrow.get(iso_example2)
                                               + timedelta(hours=1)
                                               ).isoformat()
    assert close_time(60, 200, iso_example) == (arrow.get(iso_example)
                                               + sixty_delta).isoformat()
    assert close_time(125, 200, iso_example2) == (arrow.get(iso_example2)
                                                 + onetwentyfive_delta
                                                 ).isoformat()
    assert close_time(200, 200, iso_example) == (arrow.get(iso_example)
                                                + twohundred_delta
                                                ).isoformat()
    assert close_time(550, 600, iso_example2) == (arrow.get(iso_example2)
                                                 + fivefifty_delta
                                                 ).isoformat()
    assert close_time(600, 600, iso_example) == (arrow.get(iso_example)
                                                + sixhundred_delta
                                                ).isoformat()

if __name__ == "__main__":
    nose.runmodule()
