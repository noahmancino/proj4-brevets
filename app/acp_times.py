"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
from datetime import timedelta
from collections import OrderedDict
import arrow

brevet_speed_range = OrderedDict({200: (15, 34), 400: (15, 32), 600: (15, 30),
                                  1000: (11.428, 28)})
total_brevet_time = {200: (810, 353), 300: (1200, 540),
                     400: (1620, 728), 600: (2400, 1128), 1000: (4500, 1985)}


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
       Upon failure due to invalid, it will return a negative int.
   """
    return brevet_time_helper(control_dist_km, brevet_dist_km,
                              brevet_start_time, 1)


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       Upon success, an ISO 8601 format date string indicating the control
       close time. This will be in the same time zone as the brevet start time.
       Upon failure due to invalid input, it will return a negative int.
    """
    return brevet_time_helper(control_dist_km, brevet_dist_km,
                              brevet_start_time, 0)


def brevet_time_helper(control_dist_km, brevet_dist_km, brevet_start_time,
                       table_index):
    if control_dist_km == 0:
        # You have an hour to cross the starting line.
        if not table_index:
            delta = timedelta(hours=1)
            return (arrow.get(brevet_start_time) + delta).isoformat()
        else:
            return arrow.get(brevet_start_time).isoformat()
    brevet_hours = 0
    brevet_time = arrow.get(brevet_start_time)
    if control_dist_km >= brevet_dist_km:
        if brevet_dist_km / control_dist_km <= .8333:
            return -1
            # control cannot excede total distance by more than 20%,
            # a negative int lets the flask app know what went wrong
            # which can in turn notify the frontend and user
        else:
            delta = timedelta(
                minutes=total_brevet_time[brevet_dist_km][table_index])
            brevet_time += delta
            return brevet_time.isoformat()

    last_brevet = 0
    for brevet in brevet_speed_range:
        if control_dist_km > (brevet - last_brevet):
            brevet_hours += (brevet - last_brevet) / \
                brevet_speed_range[brevet][table_index]
            control_dist_km -= (brevet - last_brevet)
            last_brevet = brevet
        else:
            brevet_hours += control_dist_km / \
                brevet_speed_range[brevet][table_index]
            break
    open_minutes = timedelta(minutes=int(round(brevet_hours * 60)))
    brevet_time += open_minutes
    return brevet_time.isoformat()
